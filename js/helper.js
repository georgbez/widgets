function sendAction( e ) {

    $modal = $('#calcModal').modal();

    // Prevent form submission
    e.preventDefault();

    // Get the form instance
    var $form = $(e.target);

    // Get the BootstrapValidator instance
    var bv = $form.data('bootstrapValidator');

    // Use Ajax to submit form data
    $.post($form.attr('action'), $form.serialize(), function (result) {

        $('#myAlert').hide();
        $('#myErrorAlert').hide();

        $modal.modal('hide');

        if (result.code == 200) {
            var $alert = $('#myAlert');
        } else {
            var $alert = $('#myErrorAlert');
        }

        $alert.find('span.text').html(result.msg);
        $alert.show();

    }, 'json');

}